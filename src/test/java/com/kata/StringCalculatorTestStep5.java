package com.kata;

import com.kata.exceptions.NegativeNumbersException;
import org.junit.jupiter.api.Test;

import static com.kata.exceptions.NegativeNumbersException.STRING_CONTAINS_NEGATIVE_NUMBERS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringCalculatorTestStep5 {

    @Test
    void should_throw_exception_when_containing_negative_number() {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN THEN
        NegativeNumbersException negativeNumbersException = assertThrows(NegativeNumbersException.class, () -> stringCalculator.add("2,0\n-3,5"));
        //THEN
        String expectedExceptionMessage = STRING_CONTAINS_NEGATIVE_NUMBERS + "-3";
        assertEquals(expectedExceptionMessage, negativeNumbersException.getMessage());
    }


    @Test
    void should_throw_exception_when_containing_negative_numbers() {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN THEN
        NegativeNumbersException negativeNumbersException = assertThrows(NegativeNumbersException.class, () -> stringCalculator.add("2,0\n-3,-5"));
        //THEN
        String expectedExceptionMessage = STRING_CONTAINS_NEGATIVE_NUMBERS + "-3 -5";
        assertEquals(expectedExceptionMessage, negativeNumbersException.getMessage());
    }
}