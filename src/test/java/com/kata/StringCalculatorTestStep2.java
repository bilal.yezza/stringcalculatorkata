package com.kata;

import com.kata.exceptions.KataException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringCalculatorTestStep2 {

    @Test
    void should_return_sum_of_more_than_two_number_string() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add("4,3,6,7");
        //THEN
        assertEquals(20, result);
    }

}