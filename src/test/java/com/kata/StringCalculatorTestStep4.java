package com.kata;

import com.kata.exceptions.KataException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringCalculatorTestStep4 {

    @Test
    void should_return_sum_of_two_number_string_with_custom_delimiter() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add("//j\n2j3j6j7");
        //THEN
        assertEquals(18, result);
    }

    @Test
    void should_return_sum_of_two_number_string_with_custom_regex_expression_delimiter() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add("//€|\\$\n2$3$6€7");
        //THEN
        assertEquals(18, result);
    }

}