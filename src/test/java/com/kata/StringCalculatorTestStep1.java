package com.kata;

import com.kata.exceptions.EndsWithDelimiterException;
import com.kata.exceptions.KataException;
import com.kata.exceptions.NotNumberException;
import com.kata.exceptions.StartsWithDelimiterException;
import org.junit.jupiter.api.Test;

import static com.kata.exceptions.EndsWithDelimiterException.ENDS_WITH_THE_DELIMITER;
import static com.kata.exceptions.NotNumberException.STRING_CONTAINS_NOT_NUMBER_ELEMENTS;
import static com.kata.exceptions.StartsWithDelimiterException.STARTS_WITH_THE_DELIMITER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringCalculatorTestStep1 {

    @Test
    void should_return_0_for_empty_string() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add("");
        //THEN
        assertEquals(0, result);
    }

    @Test
    void should_return_0_for_null_string() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add(null);
        //THEN
        assertEquals(0, result);
    }

    @Test
    void should_return_sum_of_one_number_string() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add("2");
        //THEN
        assertEquals(2, result);
    }

    @Test
    void should_return_sum_of_two_number_string() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add("2,3");
        //THEN
        assertEquals(5, result);
    }

    @Test
    void should_throw_exception_when_string_not_number() {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN THEN
        NotNumberException notNumberException =
                assertThrows(NotNumberException.class, () -> stringCalculator.add("2,notANumber"));
        //THEN
        String expectedExceptionMessage = STRING_CONTAINS_NOT_NUMBER_ELEMENTS + "notANumber";
        assertEquals(expectedExceptionMessage, notNumberException.getMessage());
    }

    // hypothesis: string mast not start with the delimiter
    @Test
    void should_throw_exception_when_starts_with_delimiter() {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN THEN
        StartsWithDelimiterException startsWithDelimiterException =
                assertThrows(StartsWithDelimiterException.class, () -> stringCalculator.add(",2"));
        //THEN
        assertEquals(STARTS_WITH_THE_DELIMITER, startsWithDelimiterException.getMessage());
    }

    // hypothesis: string mast not end with the delimiter
    @Test
    void should_throw_exception_when_ends_with_delimiter() {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN THEN
        EndsWithDelimiterException endsWithDelimiterException =
                assertThrows(EndsWithDelimiterException.class, () -> stringCalculator.add("2,"));
        //THEN
        assertEquals(ENDS_WITH_THE_DELIMITER, endsWithDelimiterException.getMessage());
    }

}