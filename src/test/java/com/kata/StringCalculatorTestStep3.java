package com.kata;

import com.kata.exceptions.KataException;
import com.kata.exceptions.NotNumberException;
import org.junit.jupiter.api.Test;

import static com.kata.exceptions.NotNumberException.STRING_CONTAINS_NOT_NUMBER_ELEMENTS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringCalculatorTestStep3 {

    @Test
    void should_return_sum_of_two_number_string_with_new_line_delimiter() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add("2\n3\n5");
        //THEN
        assertEquals(10, result);
    }

    @Test
    void should_return_sum_of_two_number_string_with_mixed_delimiter() throws KataException {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN
        int result = stringCalculator.add("2\n3,6\n7");
        //THEN
        assertEquals(18, result);
    }

    @Test
    void should_throw_exception_when_concatinating_two_delimiters() {
        //GIVEN
        StringCalculator stringCalculator = new StringCalculator();
        //WHEN THEN
        NotNumberException notNumberException = assertThrows(NotNumberException.class, () -> stringCalculator.add("2,\n3"));
        //THEN
        assertEquals(STRING_CONTAINS_NOT_NUMBER_ELEMENTS, notNumberException.getMessage());
    }

}