package com.kata;

import com.kata.exceptions.KataException;
import com.kata.operator.StringSplitter;

import java.util.List;

public class StringCalculator {

    public StringCalculator() {
    }

    public int add(String numbers) throws KataException {
        if (isEmpty(numbers)) {
            return 0;
        }
        StringSplitter stringSplitter = new StringSplitter(numbers);

        List<Integer> integers = stringSplitter.split();


        return integers.stream().reduce(0, Integer::sum);
    }

    private boolean isEmpty(String number) {
        return number == null || "".equals(number);
    }

}
