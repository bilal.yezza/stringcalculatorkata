package com.kata.exceptions;

public class StartsWithDelimiterException extends KataException {

    public static final String STARTS_WITH_THE_DELIMITER = "String starts with the delimiter";

    public StartsWithDelimiterException() {
        super(STARTS_WITH_THE_DELIMITER);
    }
}
