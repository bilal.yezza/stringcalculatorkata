package com.kata.exceptions;

public class NotNumberException extends KataException {

    public static final String STRING_CONTAINS_NOT_NUMBER_ELEMENTS = "String contains not number element : ";

    public NotNumberException(String notNumberString) {
        super(STRING_CONTAINS_NOT_NUMBER_ELEMENTS + notNumberString);
    }
}
