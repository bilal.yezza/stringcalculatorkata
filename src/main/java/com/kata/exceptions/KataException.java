package com.kata.exceptions;

public abstract class KataException extends Exception {
    public KataException(String message) {
        super(message);
    }
}
