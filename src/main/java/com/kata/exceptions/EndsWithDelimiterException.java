package com.kata.exceptions;

public class EndsWithDelimiterException extends KataException {

    public static final String ENDS_WITH_THE_DELIMITER = "String ends with the delimiter";

    public EndsWithDelimiterException() {
        super(ENDS_WITH_THE_DELIMITER);
    }
}
