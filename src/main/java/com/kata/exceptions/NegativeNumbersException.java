package com.kata.exceptions;

import java.util.List;

import static java.util.stream.Collectors.joining;

public class NegativeNumbersException extends KataException {

    public static final String STRING_CONTAINS_NEGATIVE_NUMBERS = "String contains negative numbers :";

    public NegativeNumbersException(List<Integer> negativeNumbersList) {
        super(STRING_CONTAINS_NEGATIVE_NUMBERS
                + negativeNumbersList.stream().map(Object::toString).collect(joining(" ")));
        ;
    }
}
