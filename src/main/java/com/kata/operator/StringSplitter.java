package com.kata.operator;

import com.kata.exceptions.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class StringSplitter {
    final String delimiter;
    final String numbers;

    public StringSplitter(String numbers) {
        if (numbers.startsWith("//")) {
            this.delimiter = numbers.substring(2, numbers.indexOf("\n"));
            this.numbers = numbers.substring(numbers.indexOf("\n") + 1);
        } else {
            this.delimiter = ",|\n";
            this.numbers = numbers;
        }

    }

    public List<Integer> split() throws KataException {
        checkStringStart();
        checkStringEnd();
        List<Integer> list = new ArrayList<>();
        for (String s : numbers.split(delimiter)) {
            try {
                Integer integer = Integer.valueOf(s);
                list.add(integer);
            } catch (NumberFormatException e) {
                throw new NotNumberException(s);
            }
        }
        checkNegativeNumbers(list);
        return list;
    }

    private void checkStringEnd() throws EndsWithDelimiterException {
        if (numbers.matches(".*" + delimiter + "$")) {
            throw new EndsWithDelimiterException();
        }
    }

    private void checkStringStart() throws StartsWithDelimiterException {
        if (numbers.matches("(" + delimiter + ").*")) {
            throw new StartsWithDelimiterException();
        }
    }

    private void checkNegativeNumbers(List<Integer> list) throws NegativeNumbersException {
        List<Integer> negativeNumbers = list.stream().filter(i -> i < 0).collect(toList());
        if (negativeNumbers.size() > 0) {
            throw new NegativeNumbersException(negativeNumbers);
        }
    }
}
